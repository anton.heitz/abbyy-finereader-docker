FROM ubuntu:18.04

# set path variables
RUN apt-get clean && apt-get -y update && apt-get install -y locales && locale-gen en_US.UTF-8

# path to the ABBYY engine bin
ENV LD_LIBRARY_PATH=/opt/ABBYY/FREngine12/Bin

# install reqzurements for the FRE Install
RUN apt-get update \
 && apt-get -y install ca-certificates gettext-base make g++ libgdiplus wget

# download the Engine
RUN wget -O /tmp/FRE_install.sh "https://downloads.abbyy.com/SDK/FRE/FRE12_R4U3_Linux/FRE12_R4U3_Linux_x64_build_12_4_7_1122_part_1366_26.sh?secure=wiBzgk03ODyKI_3Sc7-lHQ==&_ga=2.262455308.1826806625.1604576311-1616102258.1603984674" -q --show-progress && chmod a+x /tmp/FRE_install.sh

# copy files
COPY ABBYYLicense.ActivationToken /tmp/ABBYYLicense.ActivationToken
COPY ABBYYpassword.txt /tmp/ABBYYpassword.txt
COPY ABBYYprojectID.txt /tmp/ABBYYprojectID.txt
COPY deploy.sh /tmp/deploy.sh
COPY run.sh /tmp/run.sh

# install of FRE
RUN /tmp/deploy.sh \
    /tmp/FRE_install.sh \
    /opt/ABBYY/FREngine12 \
    /tmp/ABBYYLicense.ActivationToken \
    $(cat /tmp/ABBYYpassword.txt) \
    $(cat /tmp/ABBYYprojectID.txt)

CMD ["/tmp/run.sh"]