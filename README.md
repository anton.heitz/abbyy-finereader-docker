# Abbyy Finereader Engine 12 in a Docker Container

## Pre-Install Steps

You need a Online License from Abbyy. If you want to know how this can be aquired for your Project please ask @Tim Dilmaghani Khameneh (DE).

A online License comes with 2 parts:
 - Activation token File
 - Password File
 - A Project ID

Copy both of the Files to this directory and rename the files as followed:

 - Activation token File => `ABBYYLicense.ActivationToken`
 - Password File => `ABBYYpassword.txt`

Create a file in this directory named `ABBYYprojectID.txt` and include only the recieved ProjectID. 

For deploying via gitlab ci/cd, do not copy the password file. Instead, you can copy the String contained in the file to a gitlab secret and include it into the dockerfile instead of `$(cat /tmp/ABBYYpassword.txt)` with the gitlab functions.

You need to have docker installed on your system to run it.

## Build & Run

Build the image:

`docker build -t abbyy-docker .`

Run the Container:

`docker run -i -t abbyy-docker`

## Hints

- if the download in step 5 of the Docker build fails, get a new link to the current file from `https://www.abbyy.com/finereader-engine-downloads/linux/` (or send me a Message)


## Example CLI call

```/opt/ABBYY/FREngine12/Samples/CommandLineInterface/CommandLineInterface -if <inputfile> -f DOCX -of <outputfile>```
