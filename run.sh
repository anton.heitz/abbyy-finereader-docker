#!/bin/bash
# (c) 2018 ABBYY Production LLC

final_path=/opt/ABBYY/FREngine12

echo -- Run licensing service
LD_LIBRARY_PATH="$final_path/CommonBin/Licensing"
"$final_path/CommonBin/Licensing/LicensingService" /start

echo -- Run FRE sample
LD_LIBRARY_PATH="/opt/ABBYY/FREngine12/Bin"
$final_path/Samples/CommandLineInterface/CommandLineInterface -pi -if $final_path/Samples/SampleImages/Demo.tif -f PDF -of ./Demo.pdf

/bin/bash