#!/bin/bash
# (c) 2018 ABBYY Production LLC

install_file=$1
install_path=$2
license_path=$3
license_pass=$4
project_id=$5

echo Installing FREngine 12

$install_file -- --install-dir "$install_path" --license-path "$license_path" --license-password "$license_pass" --developer-install --project-id "$project_id"

cd "$install_path/Samples/CommandLineInterface"
make

exit 0